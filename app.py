import os
import uuid
import grpc

from flask import Flask, request, jsonify, render_template

from w2l_pb2 import Speech, Text
from w2l_pb2_grpc import SpeechToTextStub


app = Flask(__name__)


@app.route('/')
def hello_world():
    return render_template('index.html')


@app.route("/api/w2l/simple", methods=["POST"])
def run_w2l():
    host = request.form.get("host")
    port = request.form.get("port")
    file = request.files["file"]

    filename = str(uuid.uuid4())
    file.save(filename)

    with grpc.insecure_channel("{}:{}".format(host, port)) as channel:
        stub = SpeechToTextStub(channel=channel)
        # TODO send bytes directly
        with open(filename, "rb") as f:
            audio_bytes = f.read()
            result = stub.Recognize(speech_message_iterator(audio_bytes))

    os.remove(filename)

    return jsonify({
        "text": str(result.txt).replace("\n", " ")
    })


@app.route("/api/w2l/detail", methods=["POST"])
def run_w2l_detail():
    host = request.form.get("host")
    port = request.form.get("port")
    file = request.files["file"]

    filename = str(uuid.uuid4())
    file.save(filename)

    with grpc.insecure_channel("{}:{}".format(host, port)) as channel:
        stub = SpeechToTextStub(channel=channel)
        # TODO send bytes directly
        with open(filename, "rb") as f:
            audio_bytes = f.read()
            result_iter = stub.StreamRecognize(speech_message_iterator(audio_bytes))

        result_list = []
        for result in result_iter:
            result_list.append({
                "txt": result.txt,
                "start": result.start,
                "end": result.end
            })

    os.remove(filename)

    return jsonify({
        "result_list": result_list
    })


def speech_message_iterator(audio_bytes, chunk_size=1024):
    for idx in range(0, len(audio_bytes), chunk_size):
        yield Speech(
            bin=audio_bytes[idx:idx + chunk_size]
        )


if __name__ == '__main__':
    app.run()
