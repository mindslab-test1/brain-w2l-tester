FROM python:3.8-buster

RUN mkdir app
WORKDIR app

COPY . .

RUN pip install -r requirements.txt
RUN python -m grpc_tools.protoc -I. --python_out=. --grpc_python_out=. w2l.proto

EXPOSE 5000
ENTRYPOINT ["python", "app.py"]